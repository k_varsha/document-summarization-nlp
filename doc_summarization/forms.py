from .models import *
from django import forms

class UploadForm(forms.ModelForm):

	class Meta:
		model = UploadModel
		fields = ['uploadedFile']
		#db_table = "fileTable"