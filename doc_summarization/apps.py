from django.apps import AppConfig


class DocSummarizationConfig(AppConfig):
    name = 'doc_summarization'
