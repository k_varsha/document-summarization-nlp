from django.shortcuts import render
from django.http import HttpResponse

from doc_summarization.forms import UploadForm
from doc_summarization.models import UploadModel

import numpy as np
import pandas as pd
import nltk
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import re

import PyPDF2 
import textract
import io

import PIL.Image

from sklearn.metrics.pairwise import cosine_similarity

import networkx as nx

import cv2
import pytesseract
from pytesseract import image_to_string
pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract'

nltk.download('punkt')
nltk.download('stopwords')

def test_func(request):
	return render(request, "signinup.html")


def summarize_page(request):
	return render(request, "upload.html")


def save_file(request):
	save = False
	if request.method == 'POST':
		fileForm = UploadForm(request.POST, request.FILES)
		if fileForm.is_valid():
			fileForm.save()
			save = True
			ext_text, imgs = extract_text(request)
			return render(request, "downloadpage.html", {"ext_text" : ext_text, "imgs" : imgs})

	else:
		fileForm = UploadForm()
	if save == False:
		return render(request, 'upload.html', {'form' : fileForm})

def remove_stopwords(sen):
	stop_words = stopwords.words('english')
	return " ".join([word for word in sen if word not in stop_words])

def get_text(file):
	pdfFileObj = open(file,'rb')
	pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
	num_pages = pdfReader.numPages
	count = 0
	text = ""
	while count < num_pages:
	    pageObj = pdfReader.getPage(count)
	    count +=1
	    text += pageObj.extractText()
	return text

def clean_text(text):
	sentences = sent_tokenize(text)
	# remove punctuations, numbers and special characters
	clean_sentences = [re.sub('\\W+',' ', eachSent) for eachSent in sentences] 
	# make sentences lowercase
	clean_sentences = [s.lower() for s in clean_sentences]
	
	clean_sentences = [remove_stopwords(sen.split()) for sen in clean_sentences]
	return clean_sentences, sentences

def extract_text(request):
	print(request.POST.getlist('checked'))
	checked_boxes = request.POST.getlist('checked')
	
	file_path = "C:\\Users\\Varsha\\djangoproject\\django_ccproject\\DocumentSummarization\\media\\"
	file_name = request.FILES['uploadedFile'].name
	file = file_path + request.FILES['uploadedFile'].name
	static_file_path = "C:\\Users\Varsha\\djangoproject\\django_ccproject\\DocumentSummarization\\doc_summarization\\static\\"
	
	imgs = []
	if "ocr" in checked_boxes:
		img1 = cv2.imread(file)
		gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
		ret, thresh1 = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV) 
		rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (18, 18)) 
		dilation = cv2.dilate(thresh1, rect_kernel, iterations = 1) 
  
		
		contours, hierarchy = cv2.findContours(dilation, cv2.RETR_EXTERNAL,  
														 cv2.CHAIN_APPROX_NONE) 
  

		im2 = img1.copy() 
		for cnt in contours: 
			x, y, w, h = cv2.boundingRect(cnt) 
      
			# Drawing a rectangle on copied image 
			rect = cv2.rectangle(im2, (x, y), (x + w, y + h), (0, 255, 0), 2) 
      
			# Cropping the text block for giving input to OCR 
			cropped = im2[y:y + h, x:x + w] 
      
			# Apply OCR on the cropped image 
			text = pytesseract.image_to_string(cropped)
			clean_sentences, sentences = clean_text(text)
		
	if "ocr" not in checked_boxes:
		clean_sentences, sentences = clean_text(get_text(file))
	print("**** clean_sentences")

	word_embeddings = {}

	f = open('C:\\Users\\Varsha\\glove.6B.100d.txt', encoding='utf-8')
	for line in f:
	    values = line.split()
	    word = values[0]
	    coefs = np.asarray(values[1:], dtype='float32')
	    word_embeddings[word] = coefs
	f.close()

	sentence_vectors = []
	for sen in clean_sentences:
	    if len(sen) != 0:
	        v = sum([word_embeddings.get(w, np.zeros((100,))) for w in sen.split()])/(len(sen.split()) + 0.001)
	    else:
	        v = np.zeros((100,))
	    sentence_vectors.append(v)


	# create similarity matrix
	sim_mat = np.zeros([len(sentences), len(sentences)])
	for x in range(len(sentences)):
	    for y in range(len(sentences)):
	        if x != y:
	            sim_mat[x][y] = cosine_similarity(sentence_vectors[x].reshape(1,100), sentence_vectors[y].reshape(1,100))[0,0]


	
	nx_graph = nx.from_numpy_array(sim_mat)
	scores = nx.pagerank(nx_graph)
	ranked_sentences = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)
	#num_sent: number of sentences do you need as output
	ext_text = ""
	num_sent = int(request.POST.get("numoflines"))
	

	for i in range(num_sent):
		print(ranked_sentences[i][1].replace('\n', ''), end = "\n")
		ext_text += ranked_sentences[i][1].replace('\n', '')

	filetd = open("C:\\Users\\Varsha\\djangoproject\\django_ccproject\\DocumentSummarization\\media\\" + file_name[:-4] + ".txt","w")
	filetd.write(ext_text) 
	filetd.close()

	print("done Extracted!")
	
		
	if "images" in checked_boxes:
		infile_name = file
		i = 0
		
		with open(infile_name, 'rb') as in_f:
			in_pdf = PyPDF2.PdfFileReader(in_f)
			
			for page_no in range(in_pdf.getNumPages()):
				page = in_pdf.getPage(page_no)
				
				# Images are part of a page's `/Resources/XObject`
				r = page['/Resources']
				print(r)
				if '/XObject' not in r:
					continue
				for k, v in r['/XObject'].items():
					vobj = v.getObject()
					if vobj['/Subtype'] != '/Image' or '/Filter' not in vobj:
						continue
					if vobj['/Filter'] == '/FlateDecode':
               
						buf = vobj.getData()
                
						size = tuple(map(int, (vobj['/Width'], vobj['/Height'])))
						img = PIL.Image.frombytes('RGB', size, buf,
												  decoder_name='raw')

						print("got image")
						i += 1
						img.save(static_file_path + file_name[:-4] + "img" + str(i) + ".jpg")
						imgs.append(file_name[:-4] + "img" + str(i) + ".jpg")
						
						print(img)
					elif vobj['/Filter'] == '/DCTDecode':
						# A compressed image
						img = PIL.Image.open(io.BytesIO(vobj._data))
						i += 1
						img.save(static_file_path + file_name[:-4] + "img" + str(i) + ".jpg")
						imgs.append(file_name[:-4] + "img" + str(i) + ".jpg")

			
	
	print(imgs)
	return ext_text, imgs
	