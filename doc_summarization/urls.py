from django.urls import path
from django.conf.urls import url
from doc_summarization import views

urlpatterns = [
	path('', views.test_func),
	path('save', views.save_file),
	url(r'summarize/', views.save_file, name = 'summarize'),
	

]